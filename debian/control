Source: r-bioc-deseq
Maintainer: Debian R Packages Maintainers <r-pkg-team@alioth-lists.debian.net>
Uploaders: Andreas Tille <tille@debian.org>
Section: gnu-r
Testsuite: autopkgtest-pkg-r
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-r,
               r-base-dev,
               r-bioc-biocgenerics,
               r-bioc-biobase,
               r-cran-locfit,
               r-cran-lattice,
               r-bioc-genefilter,
               r-bioc-geneplotter,
               r-cran-mass,
               r-cran-rcolorbrewer,
               architecture-is-64-bit
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/r-pkg-team/r-bioc-deseq
Vcs-Git: https://salsa.debian.org/r-pkg-team/r-bioc-deseq.git
Homepage: https://bioconductor.org/packages/DESeq/
Rules-Requires-Root: no

Package: r-bioc-deseq
Architecture: any
Depends: ${R:Depends},
         ${shlibs:Depends},
         ${misc:Depends}
Recommends: ${R:Recommends}
Suggests: ${R:Suggests}
Description: GNU R differential gene expression analysis
 BioConductor package to estimate variance-mean dependence in count data from
 high-throughput sequencing assays and test for differential
 expression based on a model using the negative binomial
 distribution.
